
const { coreSchema: coreBooksSchema } = require('../app/books/books.model')
const AWS = require('aws-sdk')

const config = {
  region: 'us-west-2'
}
if (process.env === 'local') {
  config.endpoint = 'http://localhost:8000'
}
AWS.config.update(config)
const dynamodbInstance = new AWS.DynamoDB()
const diff = require('deep-diff').diff
const start = async () => {
  const dynamodb = new AWS.DynamoDB()
  console.log('Checking db tables')

  await checkTable(coreBooksSchema, dynamodb)
  process.exit(0)
}

const checkTable = async (schema) => {
  try {
    try {
      console.log(`Checking table: ${schema.TableName}`)
      const data = await dynamodbInstance.describeTable({
        TableName: schema.TableName
      }).promise()
      const { AttributeDefinitions, KeySchema, ProvisionedThroughput, TableName } = data.Table
      delete ProvisionedThroughput.LastIncreaseDateTime
      delete ProvisionedThroughput.LastDecreaseDateTime
      delete ProvisionedThroughput.NumberOfDecreasesToday
      const differrentConfig = diff(schema, { AttributeDefinitions, KeySchema, ProvisionedThroughput, TableName })

      if (differrentConfig) {
        throw new Error(`Table ${schema.TableName} has wrong configuration`)
      }
      console.log(`Finished checking table ${schema.TableName}`)
    } catch (e) {
      await dynamodbInstance.createTable(schema).promise()
      console.log(`New table ${schema.TableName} was created`)
    }
  } catch (e) {
    console.log(`There is something wrong with schema ${schema.TableName}`)
    throw e
  }
}
start()
