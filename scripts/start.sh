#!/bin/sh
export NODE_ENV=local

sls dynamodb start &
node scripts/ensureTableExistance.js
sls offline
