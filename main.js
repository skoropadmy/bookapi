'use strict'
const serverless = require('serverless-http')
const bodyParser = require('koa-bodyparser')
const Koa = require('koa')
const app = new Koa()
const bookRoutes = require('./app/books/books.router')

app.use(bodyParser())

// Error handler
app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    err.status = err.statusCode || err.status || 500
    ctx.body = err.message
    ctx.status = err.status
  }
})

app.use(bookRoutes.routes())

const handler = serverless(app)
module.exports.handler = async (event, context) => {
  const result = await handler(event, context)
  return result
}
