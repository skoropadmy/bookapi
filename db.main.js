const AWS = require('aws-sdk')

const config = {
  region: 'us-west-2'
}
if (process.env === 'local') {
  config.endpoint = process.env.DBURL
}
AWS.config.update(config)

const dynamodb = new AWS.DynamoDB.DocumentClient()
module.exports = dynamodb
