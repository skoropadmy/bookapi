const tableName = 'Books'
const dynamoDBInstance = require('../../db.main')
const { v4: uuidv4 } = require('uuid')

const createUpdatesObject = (updatesObject) => {
  const ExpressionAttributeValues = {}
  let UpdateExpression = 'SET '
  for (const key in updatesObject) {
    ExpressionAttributeValues[':' + key] = updatesObject[key]
    UpdateExpression += `${key} = :${key},`
  }
  // Remove last coma
  UpdateExpression = UpdateExpression.slice(0, -1)
  return { ExpressionAttributeValues, UpdateExpression }
}

const BooksService = {
  getBook: async (bookId) => {
    return dynamoDBInstance.get({
      TableName: tableName,
      Key: {
        uuid: bookId
      }
    }).promise()
  },

  deleteBook: async (bookId) => {
    return dynamoDBInstance.delete({
      TableName: tableName,
      Key: {
        uuid: bookId
      }
    }).promise()
  },

  updateBook: async (bookId, fieldsToUpdate) => {
    const { ExpressionAttributeValues, UpdateExpression } = createUpdatesObject(fieldsToUpdate)
    return dynamoDBInstance.update({
      TableName: tableName,
      Key: {
        uuid: bookId
      },
      ExpressionAttributeValues,
      UpdateExpression

    }).promise()
  },

  getAllBooks: async () => {
    return dynamoDBInstance.scan({ TableName: tableName }).promise()
  },

  createBook: async (book) => {
    book.uuid = uuidv4()
    return dynamoDBInstance.put({
      TableName: tableName,
      Item: book
    }).promise()
  },

  _createBookWithId: async (book, bookId) => {
    return dynamoDBInstance.put({
      TableName: tableName,
      Item: { ...book, uuid: bookId }
    }).promise()
  }
}
module.exports = BooksService
