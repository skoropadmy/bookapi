'use strict'

const router = require('koa-router')()
const BooksService = require('./books.service')

const validation = require('./books.validation')

router.post('/add', validation.addBook, async (ctx, next) => {
  await BooksService.createBook(ctx.request.body)
  ctx.response.body = 'New book was successfully added'
})

const checkExistanceOfBook = async (ctx) => {
  const book = await BooksService.getBook(ctx.params.bookId)
  if (Object.keys(book).length === 0) {
    ctx.throw(400, 'There is no books with this id')
  }
  return book
}

router.post('/delete/:bookId', validation.deleteBook, async (ctx, next) => {
  await checkExistanceOfBook(ctx)
  await BooksService.deleteBook(ctx.params.bookId)
  ctx.response.body = 'Successfully deleted a book'
})

router.put('/update/:bookId', validation.updateBook, async (ctx, next) => {
  await checkExistanceOfBook(ctx)

  await BooksService.updateBook(ctx.params.bookId, ctx.request.body)
  ctx.response.body = 'Successfully updated a book'
})

router.get('/get/:bookId', validation.getBook, async (ctx, next) => {
  const book = await checkExistanceOfBook(ctx)

  ctx.response.body = JSON.stringify(book)
})

router.get('/books', async (ctx, next) => {
  const books = await BooksService.getAllBooks()
  ctx.response.body = JSON.stringify(books)
})

module.exports = router
