const joi = require('joi')

const addBookBodySchema = joi.object().keys({
  title: joi.string().required(),
  authorName: joi.string().required(),
  releaseDate: joi.number().required()
}).required()
const updateBookBodySchema = joi.object().keys({
  title: joi.string(),
  authorName: joi.string(),
  releaseDate: joi.number()
}).required()
const bookParamsSchema = joi.object().keys({
  bookId: joi.string().guid({ version: 'uuidv4' }).required()
}).required()

const validateBody = async (ctx, schema) => {
  try {
    await schema.validate(ctx.request.body)
  } catch (e) {
    ctx.throw(400, e)
  }
}

const validateParams = async (ctx, schema) => {
  try {
    await schema.validate(ctx.params)
  } catch (e) {
    ctx.throw(400, e)
  }
}

const validation = {
  updateBook: async (ctx, next) => {
    await validateParams(ctx, bookParamsSchema)
    await validateBody(ctx, updateBookBodySchema)
    await next()
  },
  addBook: async (ctx, next) => {
    await validateBody(ctx, addBookBodySchema)
    await next()
  },
  deleteBook: async (ctx, next) => {
    await validateParams(ctx, bookParamsSchema)
    await next()
  },
  getBook: async (ctx, next) => {
    await validateParams(ctx, bookParamsSchema)
    await next()
  }
}
module.exports = validation
