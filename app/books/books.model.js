const coreSchema = {
  TableName: 'Books',
  KeySchema: [
    { AttributeName: 'uuid', KeyType: 'HASH' }
  ],
  AttributeDefinitions: [
    { AttributeName: 'uuid', AttributeType: 'S' }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
  }

}

const fullTypeSchema = {
  releaseDate: 'number',
  title: 'string',
  uuid: 'string',
  authorName: 'string'
}

module.exports = { coreSchema, fullTypeSchema }
