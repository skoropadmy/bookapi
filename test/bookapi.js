'use strict'

const mochaPlugin = require('serverless-mocha-plugin')
const expect = mochaPlugin.chai.expect
const wrapped = mochaPlugin.getWrapper('bookapi', '/main.js', 'handler')
const BooksService = require('../app/books/books.service')

const seedBook = {
  releaseDate: 2231213213,
  title: 'sfsdf',
  authorName: 'L'
}
const seedBookId = 'fdac7763-2ff7-4097-97cb-43285cc68591'
const nonExistentSeedBookId = 'fdac7588-2ff7-4097-97cb-43285cc68591'

describe('bookapi', () => {
  // TODO: Add clearing of db and normal seeding
  beforeEach(async () => {
    await BooksService._createBookWithId(seedBook, seedBookId)
  })
  afterEach(async () => {
    await BooksService.deleteBook(seedBookId)
  })

  it('On POST /add should return all books', async () => {
    const response = await wrapped.run({
      path: '/add',
      httpMethod: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        releaseDate: 2231213213,
        title: 'sfsdf',
        authorName: 'L'
      })
    })
    expect(response.body).to.be.equal('New book was successfully added')
    expect(response.statusCode).to.be.equal(200)
  })

  it('On POST /add should return error if validation fails', async () => {
    const response = await wrapped.run({
      path: '/add',
      httpMethod: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        releaseDate: 2231213213,
        title: 'sfsdf'
      })
    })
    expect(response.body).to.be.equal('child "authorName" fails because ["authorName" is required]')
    expect(response.statusCode).to.be.equal(400)
  })

  it('On GET /books should return all books', async () => {
    const response = await wrapped.run({
      path: '/books',
      httpMethod: 'GET'
    })
    expect(response.statusCode).to.be.equal(200)
  })

  it('On GET /get/:bookId should return book', async () => {
    const response = await wrapped.run({
      path: '/get/' + seedBookId,
      httpMethod: 'GET'
    })
    expect(response.body).to.be.equal('{"Item":{"title":"sfsdf","releaseDate":2231213213,"uuid":"fdac7763-2ff7-4097-97cb-43285cc68591","authorName":"L"}}')
    expect(response.statusCode).to.be.equal(200)
  })

  it('On GET /get/:bookId should return error if validation fails', async () => {
    const response = await wrapped.run({
      path: '/get/' + 'NotUUID',
      httpMethod: 'GET'
    })
    expect(response.body).to.be.equal('child "bookId" fails because ["bookId" must be a valid GUID]')
    expect(response.statusCode).to.be.equal(400)
  })

  it('On GET /get/:bookId should return error if book does not exist', async () => {
    const response = await wrapped.run({
      path: '/get/' + nonExistentSeedBookId,
      httpMethod: 'GET'
    })
    expect(response.body).to.be.equal('There is no books with this id')
    expect(response.statusCode).to.be.equal(400)
  })

  it('On PUT /update/:bookId should update book', async () => {
    const response = await wrapped.run({
      path: '/update/' + seedBookId,
      httpMethod: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        title: 'NEW'
      })
    })
    expect(response.body).to.be.equal('Successfully updated a book')
    expect(response.statusCode).to.be.equal(200)
  })

  it('On PUT /update/:bookId should return error if validation of uuid fails', async () => {
    const responseWithBadUUID = await wrapped.run({
      path: '/update/' + 'NotUUID',
      httpMethod: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        title: 'NEW'
      })
    })
    expect(responseWithBadUUID.body).to.be.equal('child "bookId" fails because ["bookId" must be a valid GUID]')
    expect(responseWithBadUUID.statusCode).to.be.equal(400)
  })

  it('On PUT /update/:bookId should return error if validation of fields fails', async () => {
    const responseWithBadBody = await wrapped.run({
      path: '/update/' + seedBookId,
      httpMethod: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        unexpectedField: 'NEW'
      })
    })
    expect(responseWithBadBody.body).to.be.equal('"unexpectedField" is not allowed')
    expect(responseWithBadBody.statusCode).to.be.equal(400)
  })

  it('On PUT /update/:bookId should return error if book does not exist', async () => {
    const response = await wrapped.run({
      path: '/update/' + nonExistentSeedBookId,
      httpMethod: 'PUT'
    })
    expect(response.body).to.be.equal('There is no books with this id')
    expect(response.statusCode).to.be.equal(400)
  })

  it('On POST /delete/:bookId should delete book', async () => {
    const response = await wrapped.run({
      path: '/delete/' + seedBookId,
      httpMethod: 'POST'
    })
    expect(response.body).to.be.equal('Successfully deleted a book')
    expect(response.statusCode).to.be.equal(200)
  })

  it('On POST /delete/:bookId should return error if validation of uuid fails', async () => {
    const response = await wrapped.run({
      path: '/delete/' + 'NotUUID',
      httpMethod: 'POST'
    })
    expect(response.body).to.be.equal('child "bookId" fails because ["bookId" must be a valid GUID]')
    expect(response.statusCode).to.be.equal(400)
  })

  it('On POST /delete/:bookId should return error if book does not exist', async () => {
    const response = await wrapped.run({
      path: '/delete/' + nonExistentSeedBookId,
      httpMethod: 'POST'
    })
    expect(response.body).to.be.equal('There is no books with this id')
    expect(response.statusCode).to.be.equal(400)
  })
})
