# BookAPI serverless project
## Prerequisites
node version 12 or higher installed

serverless installed

## Installation
```bash
npm i
serverless dynamodb install
```

## Start
```
npm start
```

## Running tests 
```
npm run test
```

## Postman documentation

https://documenter.getpostman.com/view/11727137/SzzheeQY?version=latest